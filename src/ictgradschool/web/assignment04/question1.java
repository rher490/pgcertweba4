package ictgradschool.web.assignment04;

import org.json.simple.JSONObject;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;


/**
 * Servlet implementation class question1
 */
public class question1 extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public question1() {
        super();
    }

    /**
     * We set the directory that the transactions will be written to in the
     * during servlet configuration in web.xml
     */
    protected String transactionDir = null;

    /**
     * @param servletConfig a ServletConfig object containing information gathered when
     *                      the servlet was created, including information from web.xml
     */
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        this.transactionDir = servletConfig.getInitParameter("transaction-directory");
    }
    /** init(ServletConfig) => void **/


    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // You can uncomment the following line to check the Web server
        // if necessary
        //response.getWriter().append("Served at: ").append(request.getContextPath());

        PrintWriter out = response.getWriter();

        String refNum = request.getParameter("refNum");

        // If a transaction directory hasn't been set in the web.xml, then
        // default to one within our web context (which might be in some
        // strange directory deep within .metadata in the workspace)
        if (this.transactionDir == null) {
            ServletContext servletContext = getServletContext();
            this.transactionDir = servletContext.getRealPath("/Transactions");
        }
        File jsonFile = new File(this.transactionDir, refNum + ".json");

        Map<String, String[]> infoSubmitted = request.getParameterMap();

        JSONObject allInfo = new JSONObject();

        //create billing address
        JSONObject bills = new JSONObject();
        bills.put("address", infoSubmitted.get("address")[0]);
        bills.put("postCode", infoSubmitted.get("postCode")[0]);

        //create the credit card details
        JSONObject cardDetails = new JSONObject();
        cardDetails.put("cardName", infoSubmitted.get("cardName")[0]);
        cardDetails.put("cardType", infoSubmitted.get("cardType")[0]);
        cardDetails.put("cardNo", infoSubmitted.get("cardNo1")[0] + "-" + infoSubmitted.get("cardNo2")[0] + "-" + infoSubmitted.get("cardNo3")[0] + "-" + infoSubmitted.get("cardNo4")[0]);

        //add both json file into a combined list
        allInfo.put("billingAddress", bills);
        allInfo.put("creditCard", cardDetails);

        if (saveJSONObject(jsonFile, allInfo)) {
            out.println("<div>Data saved: OK</div>");
        } else {
            out.println("<div>An error occurred.</div>");
            if (jsonFile.exists()) {
                out.println("<div>File " + jsonFile + " already exists.</div>");
            }
        }

    }
    /** doGet(HttpServletRequest, HttpServletResponse) => void **/


    /**
     * No special actions for POST so chains throught to doGet()
     *
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }
    /** doPost(HttpServletRequest, HttpServletResponse) => void **/


    /**
     * Writes the given JSONObject (in JSON format) to the specified file path
     *
     * @param file
     * @param jsonRecord
     * @return true if file written successfully, false otherwise
     */
    private boolean saveJSONObject(File file, JSONObject jsonRecord) {
        boolean statusOK = true;

        if (file.exists()) {
            System.out.println("File: " + file.toString() + " already exist.");
            return false;
        }

        String json_string = JSONObject.toJSONString(jsonRecord);
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(json_string);

        } catch (IOException e) {
            System.out.println("Error: " + json_string + " could not be saved.");
            statusOK = false;
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException e) {
                System.out.println("Closing BufferredWriter Error");
                statusOK = false;
            }
        }

        return statusOK;
    }
    /** saveJSONObject(File, JSONObject) => boolean **/
}
