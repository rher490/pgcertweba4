package ictgradschool.web.assignment04;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

/**
 * Servlet implementation class RegisterUser
 */
public class question2 extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static int COMPLETED_FORM_COUNT = 3;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public question2() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        out.println("<!doctype html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Register for ICT GradSchool Newsletter</title>");
        out.println("<meta charset='UTF-8' />");
        out.println("</head>");
        out.println("<body>");

        HttpSession session = request.getSession();

        Enumeration<String> params = request.getParameterNames();
        if (params.hasMoreElements()) {
            // This is the result of either a submit form, or clear form

            if (request.getParameter("cleardata") != null) {
                session.removeAttribute("fname");
                session.removeAttribute("lname");
                session.removeAttribute("email");
                System.out.println(session.getAttributeNames().hasMoreElements());
                createForm(session,out);

            } else {
                int numFieldsFilledIn = storeFormDataSession(request);

                if (numFieldsFilledIn == 0) {
                    out.println("<p>No information entered.</p>");
                    out.println("<p>To return to the registration page, click <a href='question2'>here</a>.</p>");
                } else if (numFieldsFilledIn != COMPLETED_FORM_COUNT) {
                    out.println("<p>The data you've entered so far has been saved.</p>");
                    out.println("<p>To continue your registration click <a href='question2'>here</a>.</p>");
                } else {
                    out.println("<p>You have been registered!</p>");
                }
            }
        } else {

            createForm(session, out);
        }
//        Properties n = getFormDataSession(session);
        // close off the HTML page
        out.println("</body>");
        out.println("</html>");
    }


    protected int storeFormDataSession(HttpServletRequest request) {

        // Needs to be completed!
        HttpSession sess = request.getSession(true);
        Map<String, String[]> m = request.getParameterMap();
        int count = 0;
        for (String key : m.keySet()) {
            String[] i = m.get(key);
//            System.out.println(i + " "+i.length);
//            System.out.println("Key: "+key+" ele: "+i[0]);
            if (i != null && i[0].length() > 0 && !key.equals("submit_button")) {
                sess.setAttribute(key, i[0]);
                count++;
            }
        }
//        System.out.println(count);

        // Store the three form fields as attributes in the session
        // (for the ones that exist)
        // Return a count of how may of the fields were stored


        return count;

    }

    protected Properties getFormDataSession(HttpSession session) {

        // Needs to be completed!

        Properties userDataProperties = new Properties();

        // Retrieves the three form fields from the session
        // and (for the ones that exist) stores them in a Properties class
        Object n = session.getAttribute("fname");
        System.out.println(n);
        if (n != null) {
            userDataProperties.setProperty("fname", n.toString());
        }
        Object m = session.getAttribute("lname");
        System.out.println(m);
        if (m != null) {
            userDataProperties.setProperty("lname", m.toString());
        }
        Object o = session.getAttribute("email");
        System.out.println(o);
        if (o != null) {
            userDataProperties.setProperty("email", o.toString());
        }


        return userDataProperties;
    }


    protected void createForm(HttpSession session, PrintWriter out) {

        Properties formFields = this.getFormDataSession(session);

        out.println("<form style='width:500px;margin:auto;' id='userform_id' name='userform' method='get' action='question2'>");
        out.println("<fieldset><legend>Register for the ICT GradSchool Newsletter:</legend>");
        out.println("<p>Form elements go here!</p>");
        // Generate appropriate input form fields for:
        //   firstname
        out.println("<label for=\"fname\">First name:</label>");

        out.print("<input class=\"form-control\" type=\"text\" id=\"fname\" name=\"fname\"");
//        System.out.println(formFields.getProperty("fname") != null);
//        System.out.println(formFields.getProperty("fname")+ "actural");
        if (formFields.getProperty("fname") != null) {
            out.print("value =\""+formFields.getProperty("fname")+"\"");
        }else{
            out.print(" placeholder=\"First Name\"");
        }
        out.print(">");
        out.println("<br>");

        //   lastname
        out.println("<label for=\"lname\">Last name:</label>");
        out.println("<input class=\"form-control\" type=\"text\" id=\"lname\" name=\"lname\"");
        if (formFields.getProperty("lname") != null) {
            out.print("value =\""+formFields.getProperty("lname")+"\"");
        }else{
            out.print(" placeholder=\"Last Name\"");
        }
        out.print(">");
        out.println("<br>");
        //   email
        out.println("<label for=\"email\">Email:</label>");
        out.println("<input class=\"form-control\" type=\"email\" id=\"email\" name=\"email\"");
        System.out.println();
        if (formFields.getProperty("email") != null) {
            out.print("value =\""+formFields.getProperty("email")+"\"");
        }else{
            out.print(" placeholder=\"Enter a valid email\"");
        }
        out.print(">");
        out.println("<br>");
        // 'Pre-fill' out a particular field in the form
        // if it exists in the variable 'formFields'


        out.println("<input type='submit' name='submit_button' id='submit_id' value='Register' /></p>");
        out.println("</fieldset>");
        out.println("</form>");
        out.println("<form  style='width:500px;margin:auto;' id='clearform_id' name='clearform' method='get' action='question2'>");
        out.println("<input type='hidden' name='cleardata' id='cleardata_id' value='clear' /></p>");
        out.println("<input type='submit' name='clear_button' id='clear_id' value='Clear Fields' /></p>");
        out.println("</form>");
    }


    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
///dont use invlidate use remove attribute one by one